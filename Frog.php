<?php
Class Frog extends Animal
{
    public $legs = "4";
    public $cold_blooded = "no";
    public $jump = "Hop hop";

    public function __construct($string)
    {
        $this->name = $string;
    }
}
?>
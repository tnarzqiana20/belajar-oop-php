<?php
    Class Ape extends Animal
    {
        public $legs = "2";
        public $cold_blooded = "no";
        public $yell = "Auoooo";

        public function __construct($string)
        {
            $this->name = $string;
        }
    }
?>